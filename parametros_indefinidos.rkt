(define (maximo lista)
  (define (auxiliar lista resultado)
    
  (cond
    ((not (list? lista)) resultado)
    ((null? lista) resultado)
    ((< resultado (car lista))
     (auxiliar (cdr lista) (car lista)))
    (else
     (auxiliar (cdr lista) resultado)
     )
    )
    )
  ;;
 (auxiliar (cdr lista) (car lista))
)
;(maximo '(1 2 3 2 1))

(define maximo2
  (lambda lista
    (maximo lista)
    )
  )
;(maximo2 1 2 3 2 1)



(define (intervalo_lista? x1 x2 lista)
  (cond
    ((not (list? lista)) #f)
    ((null? lista) #t)
    ((<= x1 (car lista) x2)
     (intervalo_lista? x1 x2 (cdr lista))
     
     )
    (else #f)
    )
  )

(define intervalo?
  (lambda (x1 x2 . lista)
    (intervalo_lista? x1 x2 lista)
    )
  )





(intervalo? 1 10 2 3 4 5 1 9)
(intervalo? 1 10 2 3 11 2 8 1)