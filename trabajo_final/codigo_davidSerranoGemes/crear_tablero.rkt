
;;Rellena el tablero de forma inicial
(define (fill-board num_tiles num_damas num_filas_damas)
  ;Crea un tablero vacio
  (define (crear_tablero_vacio num_tiles count)
    (define (create-zeros num_tiles)
      (cond
        ((= num_tiles 1) (list 0))
        (else (append (list 0) (create-zeros (- num_tiles 1))))
        )
      )
    (cond
      ((= 1 count) (list(list->vector (create-zeros num_tiles))))
      (else
        (append(list (list->vector (create-zeros num_tiles)))
        (crear_tablero_vacio num_tiles (- count 1)))
        )
      )
      
      
    )

  ;;
  ;;Rellena cada fila con las piezas correspondientes
  (define (rellenar-fila fila indice num_filas_damas num_tiles)
    (if(odd? indice)
       (cond
         ((<= indice (- num_filas_damas 1))  (do
                                           (;;var
                                            (i 0 (+ 1 i))
                                            )
                                         ((>= i num_tiles) fila );;cond
                                         ;;paso
                                         (vector-set! fila  i (if (even? i)
                                                                  -1
                                                                  -7
                                                                  )
                                                      )
                                         )
                                       )
         ((>= indice (- num_tiles num_filas_damas))  (do
                                                         (;;var
                                                          (i 0 (+ 1 i))
                                                          )
                                                       ((>= i num_tiles)fila );;cond
                                                       ;;paso
                                                       (vector-set! fila i (if (even? i)
                                                                               1
                                                                               -7
                                                                               )
                                                                    )
                                                       )
                                                     )
         (else (do
                   (;;var
                    (i 0 (+ 1 i))
                    )
                 ((>= i num_tiles) fila);;cond
                 ;;paso
                 (vector-set! fila i (if (even? i)
                                         0
                                         -7
                                         )
                              )
                 )
               )
         )
       (cond
         ((<= indice (- num_filas_damas 1))  (do
                                                 (;;var
                                                  (i 0 (+ 1 i))
                                                  )
                                               ((>= i num_tiles)fila);;cond
                                               ;;paso
                                               (vector-set! fila i (if (even? i)
                                                                       -7
                                                                       -1
                                                                       )
                                                            )
                                               )
                                             )
         
       ((>= indice (- num_tiles num_filas_damas))  (do
                                                       (;;var
                                                        (i 0 (+ 1 i))
                                                        )
                                                     ((>= i num_tiles) fila );;cond
                                                     ;;paso
                                                     (vector-set! fila i (if (even? i)
                                                                             -7
                                                                             1
                                                                             )
                                                                  )
                                                     )
                                                   )
       (else (do
                 (;;var
                  (i 0 (+ 1 i))
                  )
               ((>= i num_tiles)fila );;cond
               ;;paso
               (vector-set! fila i (if (even? i)
                                        -7
                                        0
                                        )
                            )
               )
             )
       )
       )
    )
  
       
    
  
    
  
  (define tablero (list->vector(crear_tablero_vacio num_tiles num_tiles)))
  ;;Se rellena cada fila
  (do
      (;;Variable
       (indice 0 (+ indice 1))
       )
    ((>= indice num_tiles) tablero)
    (rellenar-fila (vector-ref tablero indice) indice num_filas_damas num_tiles)
    )
 )

;;Prepara el juego y dispone las piezas
(define (initiate-game num_tiles)
  (cond
    ((<= num_tiles 5) (display" El numero de cuadros debe ser superior a 5\n"))
    (else
       
     (define num_filas_damas (- (floor(/ num_tiles 2)) 1))
     (define num_damas (if (odd? num_tiles)
                           (- (* num_filas_damas (round(/ num_tiles 2))) (floor (/ num_filas_damas 2)))
                           (* num_filas_damas (round(/ num_tiles 2)))
                           )
       )
    ; (newline)
     ;(display "Numero de piezas blancas: ")
     ;(display num_damas)
     ;(newline)
     ;(display "Numero de piezas negras: ")
     ;(display num_damas)
     ;(display "\nNumero de filas rellenas: ")
     ;(display num_filas_damas)
     ;(newline)
     (define tablero (fill-board num_tiles num_damas num_filas_damas))

     (list num_tiles num_damas num_damas num_filas_damas tablero)
     )
    )
  )


