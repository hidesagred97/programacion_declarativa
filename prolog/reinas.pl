/*     Problema de las ocho reinas   */

/*
  solucion(L).

  Predicado que comprueba su L una lista con las soluciones al problema de las ocho reinas

  Argumentos
  + L:
    - Significado: lista de pares de números que indican las filas y columnas de las reinas
    - Tipo: entrada y salida

  Variables locales
  + X:
    - Significado: número natural con valor de 1 a 8
  + Y:
    - Significado: número natural con valor de 1 a 8
  + Otras:
    - Significado: lista de pares de números que indican las filas y columnas de las reinas

  Predicados relacionados
   pertenece, no_ataca

*/

solucion([]).

solucion([X,Y|Otras]):-
		solucion(Otras),
		pertenece(Y,[1,2,3,4,5,6,7,8]),
		no_ataca([X,Y],Otras).



/*
  pertenece(X,L).

  Predicado que comprueba si X pertenece a una lista L

  Argumentos
  + X:
    - Significado: elemento
    - Tipo: entrada y salida
  + L:
    - Significado: lista de elementos
    - Tipo: entrada y salida
*/

pertenece(X,[X|_]).

pertenece(X,[_|Cola]):- pertenece(X,Cola).



/*
  no_ataca(A,B).

  Predicado que comprueba si A es una lista compuesta por dos números [X,Y] que no ataca a la lista de pares de números de B

  Argumentos
  + A:
    - Significado: lista compuesta por dos números [X,Y]
    - Tipo: entrada 
  + B:
    - Significado: lista compuesta por dos pares números [X2,Y2,X3,Y3,...]
    - Tipo: entrada

  Variables locales
  + X:
    - Significado: número natural con valor de 1 a 8
  + Y:
    - Significado: número natural con valor de 1 a 8
  + X1:
    - Significado: número natural con valor de 1 a 8
  + Y1:
    - Significado: número natural con valor de 1 a 8
  + Otras:
    - Significado: lista de pares de números que indican las filas y columnas de las reinas
*/

no_ataca(_,[]).

no_ataca([X,Y],[X1,Y1|Otras]):-
		Y =\= Y1,
		Y1 - Y =\= X1 - X,
		Y1 - Y =\= X - X1,
		no_ataca([X,Y],Otras).

/*
  tablero(L).

  Predicado que comprueba si L tiene la numeración de las filas de un tablero

  Argumentos
  + L:
    - Significado: lista de pares de números
    - Tipo: entrada 
*/


tablero([1,_,2,_,3,_,4,_,5,_,6,_,7,_,8,_]).

/* Escritura de la solucion */


/*
  escribir_solucion((L).

  Predicado que comprueba si puede escribir las casillas de un tablero

  Argumentos
  + L:
    - Significado: lista de pares de números
    - Tipo: entrada 


  Variables locales
  + X:
    - Significado: número natural con valor de 1 a 8
  + Y:
    - Significado: número natural con valor de 1 a 
  + Cola:
    - Significado: lista de pares de números

 Predicado_relacionado
   write
*/

escribir_solucion([]).

escribir_solucion([X, Y | Cola]):-
         write(' Casilla: '), write(X),write(' , '), write(Y),
         nl,
         escribir_solucion(Cola).
         
         
/*
 *  Para obtener las soluciones:
 *
 * ?- tablero(S), solucion(S), escribir_solucion(S).
 *
 * Al teclear "punto y coma" (;) van a apareciendo las soluciones
 *
 */
 
  
       
