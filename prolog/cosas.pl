/*
  tiene(Persona,Cosa)

  Predicado que indica si una Persona tiene una Cosa

  Argumentos
  + Persona: 
     - Significado: nombre de una persona
     - Tipo: entrada y salida
  + Cosa: 
     - Significado: nombre de una cosa
     - Tipo: entrada y salida
*/

tiene(maria, libro).
tiene(maria, cuaderno).
tiene(juan, cuaderno).
tiene(juan, bicicleta).
