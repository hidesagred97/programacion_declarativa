/**
Arbol de ejemplo
[5, [3, [1,[],[]] , [] ] , [8, [6, [], [7,[],[]] ] ,[] ] ]


**/
/*
escribir_prefijo([Nodo,Arbol1,Arbol2])
funcion que escribe un arbol en modod prefijo 
    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
Todos son de entrada y de salida

*/
escribir_prefijo([]).
escribir_prefijo([Nodo,Arbol1,Arbol2]):-write(Nodo),
										write(" "),
										escribir_prefijo(Arbol1),
										escribir_prefijo(Arbol2).




/*
escribir_infijo([Nodo,Arbol1,Arbol2])
funcion que escribe un arbol en modod infikjo
    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
Todos son de entrada y de salida

*/
escribir_infijo([]).
escribir_infijo([Nodo,Arbol1,Arbol2]):-	escribir_infijo(Arbol1),
										write(Nodo),
										write(" "),
										escribir_infijo(Arbol2).
										


/*
escribir_sufijo([Nodo,Arbol1,Arbol2])
funcion que escribe un arbol en modod sufijo 
    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
Todos son de entrada y de salida

*/

escribir_sufijo([]).
escribir_sufijo([Nodo,Arbol1,Arbol2]):-	escribir_sufijo(Arbol1),
										escribir_sufijo(Arbol2),
										write(Nodo),
										write(" ").										

/*

profundidad_arbol([Nodo,Arbol1, Arbol2],P)
calcula la profundidad maxima de un arbol

    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
    P: profundidad del arbol
Todos son de entrada y de salida
*/

profundidad_arbol([],P):-P is 0.
profundidad_arbol([_,Arbol1,Arbol2],P):-    profundidad_arbol(Arbol1,P1),
											profundidad_arbol(Arbol2,P2),
											P_aux is max(P1,P2),
			
            								P is P_aux + 1.

/*

existe([Nodo,Arbol1, Arbol2],Busca)
comprueba si un un elemento existe en un arbol

    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
    Busca: elemento que buscamos en el arbol
Todos son de entrada y de salida
*/

existe([],_):-fail.
existe([Nodo,_,_],Nodo).
existe([_,Arbol1,Arbol2],Busca):-existe(Arbol1,Busca);existe(Arbol2,Busca).	


/*

numero_nodos([Nodo,Arbol1, Arbol2],N)
calcula el numero de nodos en un arbol

    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
    N: numero de nodos
Todos son de entrada y de salida
*/

numero_nodos([],N):-N is 0.
numero_nodos([_,Arbol1,Arbol2],N):-	numero_nodos(Arbol1,N1),
									numero_nodos(Arbol2,N2),
									N_aux is N1 + N2,
									N is N_aux + 1.



/*

numero_hojas([Nodo,Arbol1, Arbol2],N)
calcula el numero de nodos  hoja en un arbol

    Nodo: elemento del arbol
    Arbol1: arbol izq
    Arbol2: arbol derecho
    N: numero de nodos hoja
Todos son de entrada y de salida
*/


numero_hojas([],N):-N is 0.
numero_hojas([_,[],[]],N):-N is 1.
numero_hojas([_,Arbol1,Arbol2],N):-	numero_hojas(Arbol1,N1),
									numero_hojas(Arbol2,N2),
									N is N1 + N2.
									


/*
PAra redirigir la salida a un archivo usamos tell("NombreFichero")

tell('fichero.txt'),
numero_nodos([5, [3, [1,[],[]] , [] ] , [8, [6, [], [7,[],[]] ] ,[] ] ],N),
write(N),
told.

*/
