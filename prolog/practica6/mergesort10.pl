concatenar([],L,L).

concatenar([X|L1],L2,[X|L3]):- concatenar(L1,L2,L3).
/*

split(L,L1,L2)
Divide una lista en dos listas, en una los elementos en posiciones pares y en la otra las impares
L: lista originak
L1: lista de impares
L2: lista de posiciones pares

Ambos son de entrada y salida
*/
split([],[],[]).
split([X],[X],[]).
split([X,Y|Cola],[X|L1],[Y|L2]):- split(Cola,L1,L2).


/*
Une dos listas en una lista ordenada
merge(L1,L2,L)
L1 yL2: listas que vamos a unir
L: lista ordenada resultante

Ambos son de entrada y salida
*/
merge([],[],[]).
merge([X],[],[X]).
merge([],[X],[X]).
merge([Cabeza|Cola],[],[Cabeza|Cola]).
merge([Cabeza|Cola],[Cabeza|Cola],[]).
merge([Cabeza1|Cola1],[Cabeza2|Cola2],L):-	Cabeza1 < Cabeza2,
											merge(Cola1,[Cabeza2|Cola2],L1),
											concatenar([Cabeza1],L1,L).

merge([Cabeza1|Cola1],[Cabeza2|Cola2],L):-	Cabeza1 >= Cabeza2,
											merge([Cabeza1|Cola1],Cola2,L1),
											concatenar([Cabeza2],L1,L).

/*
mergesort(L,R)
Metodo de ordenación de una Lista, de modo  que el menor queda en la primera posición
L: lista a ordenar
R: lista ordenada

Ambos son de entrada y salida
*/
mergesort([],[]).

mergesort([X],[X]).

mergesort(L,R):- 	split(L,L1,L2),
					mergesort(L1,L1_ord),
					mergesort(L2,L2_ord),
					merge(L1_ord,L2_ord,R).

