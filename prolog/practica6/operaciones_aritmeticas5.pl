

/*
area_circulo(Radio,A)

Calcula el area de un circulo dado un radio.

	Argumento
           Radio
			Significado: distancia desde el centro del circulo a cualquier punto del mismo
			Entrada y salida
           A:
            Significado: area del circulo
            Entrada y salida

*/


area_circulo(Radio,A):-A is pi * Radio*Radio.


/*
area_trapecio(B_mayor,B_menor,Altura,A)

Calcula el area de un circulo dado un radio.

	Argumento
           B_mayor: Lado paralelo más grande
           B_menor: lado paralelo más pequeño
           Altura: distancia entre los lados paralelos
           A:
            Significado: area del circulo


           Todas son de Entrada y salida

*/
area_trapecio(B_mayor,B_menor,Altura,A):-A is Altura * (B_menor+B_mayor)/2.




/*
producto_intermedio(N1,N2,R)

    Argumentos
    N1: Uno de los numeros a partir del cual queremos multiplicar
    N2: El otro
    Los extremos son inclusivos
    Todos los parametros son de entrada y salida.
*/

producto_intermedio(N1,N2,R):-N1<N2,
							X is N1 + 1,
							producto_intermedio(X,N2,R2),
							R is N1 * R2.
producto_intermedio(N1,N2,R):-N1>N2,
							X is N1 - 1,
							producto_intermedio(X,N2,R2),
							R is N1 * R2.
producto_intermedio(N1,N2,R):-N1=N2,
							R is N2.							

