
concatenar([],L,L).
concatenar([X | L1], L2, [X | L3]):-
									concatenar(L1,L2,L3).



/*

crear(N,L)
    Creamos una lista de elementos que van desde 0 hasta N de uno en uno
    Argumentos:
        N: Numero de elementos que tiene la lista
        L: lista que se crea de 0 a N
    Ambos son de entrada y salida
*/
crear(0,[0]).
crear(N,L):-N1 is N-1,
			crear(N1,X),
			concatenar(X,[N],L).
