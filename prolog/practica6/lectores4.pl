lector(nombre("Ana","Garrido","Aguirre"),mujer,31).
lector(nombre("Marta","Cantero","Lasa"),mujer,20).
lector(nombre("Rodrigo","Duque","Soto"),hombre,30).
lector(nombre("Rodrigo","Ramos","Lara"),hombre,32).
lector(nombre("Luisa","Ramos","Duque"),mujer,42).






hay_lectores():-lector(nombre(_,_,_),_,_).
quien_es_lector(X):-lector(X,_,_).
quien_es_lector_hombre(X):-lector(X,hombre,_).
quien_es_lector_mujer(X):-lector(X,mujer,_).

lectores_mismo_nombre():-lector(nombre(X,A1,A2),_,_),
						lector(nombre(X,A3,A4),_,_),
						A3\=A1,
						A4\=A2.


apellidos_repetidos():-lector(nombre(N1,A1,A2),_,_),
						lector(nombre(N2,A3,A4),_,_),
						(N1\=N2;N1=N2),
						(A1=A3;A1=A4;A2=A3;A2=A4).



/*
bagof(X,quien_es_lector_hombre(X),L).

*/