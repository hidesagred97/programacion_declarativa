es_primo(_,1).

es_primo(X,N):- Resto is X mod N,
				Resto \= 0,
				N1 is N - 1,
				es_primo(X,N1).
/*

    primo(X)
    COmprueba si X es primo
    X: es el numero que queremos comprobar si es primo
    Es tanto de entrada como de salida
*/

primo(X):-	X2 is round(sqrt(X)),
				es_primo(X,X2).
