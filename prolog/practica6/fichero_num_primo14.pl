
es_primo(_,1).

es_primo(X,N):- Resto is X mod N,
				Resto \= 0,
				N1 is N - 1,
				es_primo(X,N1).


primo(end_of_file):-!.
primo(X):-	X2 is round(sqrt(X)),
				es_primo(X,X2),!,
				write(X),
				write('.'),
				nl.

no_primo(end_of_file):-!.
no_primo(X):-	X2 is round(sqrt(X)),
				not(es_primo(X,X2)),!.				

/*
program:
Escribe en un fichero los numeros primos leidos de un fichero de numeros
No tiene parametros
Los nombres de los ficheros deben escribirse entre comillas
*/

program:- 
       display("Fichero de lectura, entre comillas simples-> "),
		   read(Fich_entrada),
		   open(Fich_entrada,read,Stream),
		display("Fichero de escritura, entre comillas simples-> "),
		   read(Fich_salida),
		   tell(Fich_salida),

       repeat,
			read(Stream,Numero),
    	   	(primo(Numero);no_primo(Numero)),
    	   	Numero==end_of_file,
    	told.
      		
      		





