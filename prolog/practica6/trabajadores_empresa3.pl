/*
encargado_de_tarea(trabajador,tarea)

Indica si un trabajador esta encargado de una tarea

	Argumentos
		trabajador:
			Significado: nombre de un trabajador
			Entrada y salida
		tarea:
			Significado: nombre de una tarea
			Entrada y salida


*/

encargado_de_tarea(miguel,admision).
encargado_de_tarea(miguel,control).
encargado_de_tarea(miguel,vigilancia).
encargado_de_tarea(ricardo,planificacion).
encargado_de_tarea(ricardo,asesoramiento).
encargado_de_tarea(alicia,direccion).
encargado_de_tarea(alicia,control).



/*
encargada(tarea)

Indica alguien esta encargado de la tarea x

	Argumentos
		tarea:
			Significado: nombre de una tarea
        	Entrada y salida


*/


encargada(X):-encargado_de_tarea(_,X).


/*
comparten_tarea(P1,P2)

Indica si dos trabajadores comparten una tarea

	Argumentos
           P1
			Significado: nombre de un trabajador
			Entrada y salida
           P2
			Significado: nombre de un trabajador
			Entrada y salida


*/
comparten_tarea(P1,P2):-encargado_de_tarea(P1,X),
						encargado_de_tarea(P2,X).
