donante(persona("juan","campos","ruiz"),a,positivo).
donante(persona("ana","lara","silva"),ab,negativo).
donante(persona("luis","luna","pachero"),ab,negativo).
donante(persona("david","serrano","gemes"),0,positivo).

/*
donar(P1,P2)
Comprueba si una persona puede donar a otra
    P1: persona que dona
    P2: persona que recibe la sangre

Ambos parametros son de entraday de salida
*/

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),0,negativo),
												donante(persona(N2,A3,A4),_,_).

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),0,positivo),
												donante(persona(N2,A3,A4),_,positivo).


donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),a,negativo),
												(donante(persona(N2,A3,A4),a,_);donante(persona(N2,A3,A4),ab,_)).

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),b,_),
												(donante(persona(N2,A3,A4),b,_);donante(persona(N2,A3,A4),ab,_)).


donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),a,positivo),
												(donante(persona(N2,A3,A4),a,positivo);donante(persona(N2,A3,A4),ab,positivo)).

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),b,positivo),
												(donante(persona(N2,A3,A4),b,positivo);donante(persona(N2,A3,A4),ab),positivo).

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),ab,negativo),
												donante(persona(N2,A3,A4),ab,_).

donar(persona(N1,A1,A2),persona(N2,A3,A4)):-	donante(persona(N1,A1,A2),ab,positivo),
												donante(persona(N2,A3,A4),ab,positivo).												


contar([],N):-N is 0.
contar([_|Cola],N):-contar(Cola,N1),
						N is N1 + 1.


/*
contar_por_grupo_y_factor(Grupo,Factor,N)
comprueba cuantas personas tienen una determinada combinacion de grupo y factor
Grupo: grupo sanguineo
Factor: Rh
N: numero de personas que lo tienen

Todos los parametros son de entrada y de salida
*/
contar_por_grupo_y_factor(Grupo,Factor,N):-bagof(X,donante(X,Grupo,Factor),L),
										contar(L,N).

get_todos_donantes_grupo_factor(Grupo,Factor,L):-bagof(N/A1/A2/Grupo/Factor,donante(persona(N,A1,A2),Grupo,Factor),L).


escribir_elementos_lista_fichero([],_).
escribir_elementos_lista_fichero([Cabeza|Cola],X):- 	write(X,Cabeza),
														write(X,"\n"),
												escribir_elementos_lista_fichero(Cola,X).
/*
escribir_donantes_grupo_y_factor()
Funcion que escribe todos los donante sde un grupo y factor en un fichero.

No tiene paramentros

*/
escribir_donantes_grupo_y_factor():-write("Escribe un grupo sanguineo: a/b/ab/0 : "),
									read(Grupo),
									write("Escribe un factor rh: positivo/negativo: "),
									read(Rh),
									write("Escribe el nombre de un fichero, con comillas simples: "),
									read(Fich_name),
									get_todos_donantes_grupo_factor(Grupo,Rh,L),
									open(Fich_name,write,X),
									escribir_elementos_lista_fichero(L,X),
									close(X).
