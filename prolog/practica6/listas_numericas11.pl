contar([],N):-N is 0.
contar([_|Cola],N):-contar(Cola,N1),
						N is N1 + 1.

sumar_lista([],S):- S is 0.
sumar_lista([Cabeza|Cola],S):-	sumar_lista(Cola,S1),
								S is S1 + Cabeza.

/*
media_lista(L,M): 
Calcula la meida de una lista
    L: lista de la qu ese calcula una media
    M: media

Ambos parametros son de entrada y de salida
*/
media_lista(L,M):-
				contar(L,N),
				sumar_lista(L,S),
				M is S/N.

/*
maximo_lista(L,M)
L: lista de la que se calcula el maximo
M: maximo de la lista

Ambos parametros son de entrada y de salida
*/
maximo_lista([],Mtemp,M):-M is Mtemp.
maximo_lista([Cabeza|Cola],Mtemp,M):- Mtemp >= Cabeza,
									maximo_lista(Cola,Mtemp,M).
maximo_lista([Cabeza|Cola],Mtemp,M):- Mtemp < Cabeza,
									maximo_lista(Cola,Cabeza,M).


maximo_lista([Cabeza|Cola],M):-maximo_lista(Cola,Cabeza,M).







/*

NECESARIO PARA ORDENAR

*/

concatenar([],L,L).

concatenar([X|L1],L2,[X|L3]):- concatenar(L1,L2,L3).


split([],[],[]).
split([X],[X],[]).
split([X,Y|Cola],[X|L1],[Y|L2]):- split(Cola,L1,L2).

merge([],[],[]).
merge([X],[],[X]).
merge([],[X],[X]).
merge([Cabeza|Cola],[],[Cabeza|Cola]).
merge([Cabeza|Cola],[Cabeza|Cola],[]).
merge([Cabeza1|Cola1],[Cabeza2|Cola2],L):-	Cabeza1 < Cabeza2,
											merge(Cola1,[Cabeza2|Cola2],L1),
											concatenar([Cabeza1],L1,L).

merge([Cabeza1|Cola1],[Cabeza2|Cola2],L):-	Cabeza1 >= Cabeza2,
											merge([Cabeza1|Cola1],Cola2,L1),
											concatenar([Cabeza2],L1,L).

mergesort([],[]).

mergesort([X],[X]).

mergesort(L,R):- 	split(L,L1,L2),
					mergesort(L1,L1_ord),
					mergesort(L2,L2_ord),
					merge(L1_ord,L2_ord,R).






eliminar_ultimo_elemento([],[]).
eliminar_ultimo_elemento([_],[]).
eliminar_ultimo_elemento([Cabeza|Cola],L):-eliminar_ultimo_elemento(Cola,L1),
											concatenar([Cabeza],L1,L).

/*

mediana_lista(L,M)
Calcula el elemento medio de una lista ordenada
L: lista de la que se calcula la mediana
M: mediana
Ambos parametros son de entrada y de salida
*/
mediana_lista_ord([X],X).

mediana_lista_ord([X,Y],Media):-			Media is (X + Y )/ 2.

mediana_lista_ord([_|Cola],M):-	eliminar_ultimo_elemento(Cola,Cola1),
										mediana_lista_ord(Cola1,M).

mediana_lista([Cabeza|Cola],M):-mergesort([Cabeza|Cola],L_ord),
										mediana_lista_ord(L_ord,M).

