/*
es_lista(L)
Compruba si l es una lista
    L: Elemento que puede ser una lisat
    PArametro de entrada
*/

es_lista([]).
es_lista([_|Cola]):-es_lista(Cola).

concatenar([],L,L).

concatenar([X|L1],L2,[X|L3]):- concatenar(L1,L2,L3).


/*
invertir(L)
    Invierte la lista L
    L:lista que se va a invertir. Puede tener sublistas
L es un parametro de entrada

*/
invertir([],[]).
invertir([Cabeza|Cola],R):-invertir(Cabeza,R1),
							invertir(Cola,R2),
							concatenar(R2,[R1],R).

invertir([Cabeza|Cola],R):-invertir(Cola,R1),
							concatenar(R1,[Cabeza],R).
