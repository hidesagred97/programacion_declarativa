monumento("Mezquita","Cordoba","Arabe").
monumento("Medina Azahara","Cordoba","Arabe").
monumento("Catedral","Santiago de Compostela","Romanico").
/*

Conta(L,N):
    Cuenta el numero de elementos que tiene una lista
    L: lista que contamos
    N: numero de elementos
    Ambos son de entrada y salida
*/
contar([],N):-N is 0.
contar([_|Cola],N):-contar(Cola,N1),
						N is N1 + 1.
						
/*
cuenta el numero de monumentos que tiene una ciudad
contar_monumentos(Ciudad,N)
    Ciudad: cadena que indica la ciudad de la contamos el numero de monumentos
    N: numero de monumentos que tiene una ciudad

    Ambos son de entrada y salida
*/
contar_monumentos(Ciudad,N):-bagof(M,monumento(M,Ciudad,_),L),
							contar(L,N).
