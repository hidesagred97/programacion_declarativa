/*
  hombre(Persona)

  Predicado que indica si una Persona es un hombre

  Argumento
  + Persona
    - Significado: nombre de una persona
    - Tipo: entrada y salida
*/

hombre(platon).
hombre(socrates).
hombre(aristoteles).


/*
  mortal(X)

  Predicado que indica si X es mortal; será mortal si X es un hombre

  Argumento
  + X
    - Significado: nombre de una persona
    - Tipo: entrada y salida

  Predicados relacionados
  + hombre
*/
mortal(X):-hombre(X).
