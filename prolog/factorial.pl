/*
  factorial(N,R)

  Predicado que comprueba si R es el factorial del número N

  Argumentos
  + N:
    - Significado: número
    - Tipo: entrada
  + R: 
	- Significado: número
    - Tipo: entrada y salida

  Variables locales
   + N1:
    - Significado: número
   + R1:
    - Significado: número
*/


factorial(0,1).

factorial(N,R):- 
		N1 is N - 1,
		factorial(N1,R1),
		R is N * R1.

/*
  Define al predicado factorial como operador infijo.
  Ejemplo:

   3 ! R.
*/
!(X,R):-factorial(X,R).
?- op(150,xfy,!).
