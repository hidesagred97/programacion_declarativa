/*
  concatenar(A,B,R).

  Predicado que comprueba R es la concantenación de las listas A y B

  Argumentos
  + A:
    - Significado: lista de elementos
    - Tipo: entrada y salida
  + B:
    - Significado: lista de elementos
    - Tipo: entrada y salida

  + R:
    - Significado: lista de elementos
    - Tipo: entrada y salida

  Variables locales
  + X:
    - Significado: elemento; cabeza de la lista A
 + L1:
    - Significado: lista de elementos; cola de la lista A.
 + L2:
    - Significado: lista de elementos; lista B.
  + L3:
    - Significado: lista de elementos; cola la lista R.
*/

concatenar([],L,L).

concatenar([X|L1],L2,[X|L3]):- concatenar(L1,L2,L3).


/*
  pertenece(X,L).

  Predicado que comprueba si X pertenece a una lista L: X pertenece a L si L se puede obtener mediante una concatenación de dos listas, siendo X la cabeza de la segunda lista.

  Argumentos
  + X:
    - Significado: elemento
    - Tipo: entrada y salida
  + L:
    - Significado: lista de elementos
    - Tipo: entrada y salida


  Predicado relacionado
   concatenar
*/


pertenece(X,L):- concatenar(_,[X|_], L).
