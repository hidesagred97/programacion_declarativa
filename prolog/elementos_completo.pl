es_lista([]).
es_lista([_|Cola]):-
	es_lista(Cola).




contar_elementos([],0).

contar_elementos([Cabeza|Cola],N):-
	es_lista(Cabeza),
	contar_elementos(Cabeza,N1),
	contar_elementos(Cola,N2),
	N is N1+N2.

contar_elementos([Cabeza|Cola],N):-
	not(es_lista(Cabeza)),
	contar_elementos(Cabeza,N1),
	contar_elementos(Cola,N2),
	N is N1+N2.


/*   Otra forma*/

contar_elementos([Cabeza|Cola],N):-
	contar_elementos(Cabeza,N1),
	contar_elementos(Cola,N2),
	N is N1 + N2.

contar_elementos(_,1).